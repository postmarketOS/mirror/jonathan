import urllib.parse

import requests


def get_issue(repo, issue):
    instance, project = repo.split("/", maxsplit=1)
    project = urllib.parse.quote_plus(project)
    url = f"https://{instance}/api/v4/projects/{project}/issues/{issue}"

    result = requests.get(url)
    if result.status_code != 200:
        return None
    result = result.json()

    display_state = {
        "opened": "open",
    }

    if result["state"] in display_state:
        result["state"] = display_state[result["state"]]

    return {
        "title": result["title"],
        "state": result["state"],
        "url": result["web_url"],
    }


def get_merge_request(repo, mr):
    instance, project = repo.split("/", maxsplit=1)
    project = urllib.parse.quote_plus(project)
    url = f"https://{instance}/api/v4/projects/{project}/merge_requests/{mr}"

    result = requests.get(url)
    if result.status_code != 200:
        return None
    result = result.json()

    return {
        "title": result["title"],
        "state": result["state"],
        "url": result["web_url"],
    }


if __name__ == "__main__":
    print(get_issue("gitlab.com/postmarketOS/pmaports", 1474))
