import configparser
import json

_db = configparser.ConfigParser()
_path = None


def open_db(path):
    global _path
    global _db
    _path = path
    _db.read(path)


def save():
    with open(_path, "w") as handle:
        _db.write(handle)


def get(section, key, default=None):
    if section not in _db:
        return default
    if key not in _db[section]:
        return default
    return _db[section][key]


def set_val(section, key, value):
    if section not in _db:
        _db.add_section(section)

    _db[section][key] = str(value)


def add(section, key, value):
    if section not in _db:
        _db.add_section(section)

    if key not in _db[section]:
        _db[section][key] = "[]"

    raw = set(json.loads(_db[section][key]))
    raw.add(value)
    _db[section][key] = json.dumps(list(raw))


def remove(section, key, value=None):
    if section not in _db:
        return

    if key not in _db[section]:
        return

    if value:
        raw = json.loads(_db[section][key])
        raw.remove(value)
        _db[section][key] = json.dumps(raw)
    else:
        _db[section].pop(key)


def list_values(section, key):
    if section not in _db:
        return []

    if key not in _db[section]:
        return []

    return json.loads(_db[section][key])


def list_keys(section):
    if section not in _db:
        return []

    return _db[section].keys()


def store_alias(alias, room_id):
    # ConfigParser doesn't like most chars in a room alias, replace them
    # This does #room:server.org -> room@server.org which is a safe key for ConfigParser
    safe = alias.replace("#", "").replace(":", "@")
    set_val("matrix", safe, room_id)


def get_room_id(alias):
    safe = alias.replace("#", "").replace(":", "@")
    return get("matrix", safe, alias)
