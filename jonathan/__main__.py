import asyncio
import collections
import hashlib
import logging
import argparse
import configparser
import re
import miniirc

import jonathan.database as database
from jonathan.irc import ClientIRC

from nio import (
    AsyncClient,
    RoomMessageText,
    LoginError,
    InviteMemberEvent,
    PowerLevelsEvent,
    RoomMessageFile,
    DownloadError,
    RoomKickError,
)

from jonathan.gitlab import get_merge_request, get_issue

config = None
_power_levels = {}
file_history = collections.deque(maxlen=10)

logging.basicConfig(level=logging.INFO)

def is_user_on_irc(user):
    if not user.startswith("@") or user.startswith(config["admin"]["appserviceirc_prefix"]):
        return True
    return False

def get_irc_nick(user):
    return user.removeprefix(config["admin"]["appserviceirc_prefix"]).removesuffix(config["admin"]["appserviceirc_suffix"])

async def send(room_id, message):
    await client_matrix.room_send(
        room_id,
        message_type="m.room.message",
        content={"msgtype": "m.notice", "body": message},
    )


async def on_power_change(room, event):
    # Keep the state of all the power levels in all joined rooms
    global _power_levels

    logging.info(f"Got new power levels for {room.room_id} ({room.canonical_alias})")
    if room.room_id in _power_levels:
        _power_levels[room.room_id].update(event.power_levels)
    else:
        _power_levels[room.room_id] = event.power_levels


async def power_level_update(room_id, user, level):
    if room_id not in _power_levels:
        logging.error(f"Could not update power levels for uncached room: '{room_id}'")
        return

    power_levels = _power_levels[room_id]
    power_levels.users[user] = level

    content = {
        "events": power_levels.events,
        "users": power_levels.users,
        "ban": power_levels.defaults.ban,
        "invite": power_levels.defaults.invite,
        "kick": power_levels.defaults.kick,
        "redact": power_levels.defaults.redact,
        "state_default": power_levels.defaults.state_default,
        "events_default": power_levels.defaults.events_default,
        "users_default": power_levels.defaults.users_default,
    }
    logging.info(f"Updating power levels for {room_id}")
    res = await client_matrix.room_put_state(room_id, event_type="m.room.power_levels", content=content)
    logging.info(res)

async def sync(room):
    # Sync rooms
    is_joined = set()
    joined_rooms = await client_matrix.joined_rooms()
    for room in joined_rooms.rooms:
        is_joined.add(room)

    # Sync power levels
    members = await client_matrix.joined_members(config["admin"]["adminroom"])
    for member in members.members:
        if member.user_id == config["connection"]["matrix_account"]:
            continue


async def on_invite(room, event):
    # Invite event gets both the invite itself and the an event for every member in the room
    if not event.membership == "invite":
        return

    # Only allow getting invited in the admin room for now
    if room.room_id != config["admin"]["adminroom"]:
        logging.error(f"Invited into unknown room: {room.room_id} by {event.sender}")
        return

    logging.info(f"Joining on invite: {room.room_id}")
    await client_matrix.join(room.room_id)


async def on_file(room, event):
    global file_history
    # Don't react on old messages
    last_ts = int(database.get("state", "last_ts", 0))
    if event.server_timestamp <= last_ts:
        return

    server, media_id = event.url.replace("mxc://", "").split("/", maxsplit=1)
    file = await client_matrix.download(server, media_id)

    # Just skip the file if it fails to download
    if isinstance(file, DownloadError):
        logging.warning(f"Failed to download a file: {file}")
        return

    hash = hashlib.sha1(file.body).hexdigest()

    # Keep a history of the last 10 files uploaded, for banning
    file_history.appendleft((hash, file.filename, event.sender, room.canonical_alias))

    bans = database.list_values("state", "fileban")
    if hash in bans:
        await send(
            config["admin"]["adminroom"],
            f"Banned file posted by {event.sender} in {room.canonical_alias}",
        )


async def on_message(room, event):
    # Don't react on own messages
    if event.sender == client_matrix.user_id:
        return

    if room.canonical_alias:
        database.store_alias(room.canonical_alias, room.room_id)

    # Don't react on old messages
    last_ts = int(database.get("state", "last_ts", 0))
    if event.server_timestamp <= last_ts:
        return

    database.set_val("state", "last_ts", event.server_timestamp)
    database.save()

    # Handle admin room messages
    if room.room_id == config["admin"]["adminroom"] and event.body.startswith("!"):
        await on_admin_message(room, event)
        return

    # Handle normal messages for PMA!\d+ support
    await on_normal_message(room, event)


async def on_normal_message(room, event):
    message = event.body
    refs = set()

    if event.source['content'].get('m.relates_to'): # Don't react on quoted messages
        # https://spec.matrix.org/v1.11/client-server-api/#fallbacks-for-rich-replies
        for line in message.split("\n"):
            if line.startswith("> "):
                message.replace(line, "")
            else:
                break

    for key in database.list_keys("alias"):
        mrs = re.findall(rf"{key}!(\d+)", message)
        issues = re.findall(rf"{key}#(\d+)", message)
        for mr in mrs:
            refs.add((key, "!", int(mr)))
        for issue in issues:
            refs.add((key, "#", int(issue)))

    if len(refs) > 5:
        # Avoid ddossing gitlab when someone tries to add 100 issue numbers to a single message
        return

    response_html = "<ul>" if len(refs) > 1 else ""
    response_text = ""
    send_response = False
    for key, type, num in refs:
        repo = database.get("alias", key)
        if type == "!":
            info = get_merge_request(repo, num)
        elif type == "#":
            info = get_issue(repo, num)

        if info is not None:
            send_response = True
            if len(refs) > 1:
                response_html += "<li>"
                response_text += "* "
            response_html += (
                f'[{type}{num}, {info["state"]}] <a href="{info["url"]}">{info["title"]}</a>'
            )
            response_text += f'[{type}{num}, {info["state"]}] {info["title"]}: {info["url"]}\n'
            if len(refs) > 1:
                response_html += "</li>"

    if send_response:
        if len(refs) > 1:
            response_html += "</ul>"
        await client_matrix.room_send(
            room.room_id,
            message_type="m.room.message",
            content={
                "msgtype": "m.notice",
                "body": response_text,
                "format": "org.matrix.custom.html",
                "formatted_body": response_html,
            },
        )


async def on_admin_message(room, event):
    part = event.body.split()
    cmd = part[0].replace("!", "")

    if cmd == "join" and len(part) == 3:
        if part[2].lower() == "matrix":
            await send(room.room_id, f"Adding {part[1]} to the managed matrix room list")
            database.add("matrix", "managed", part[1])
            await client_matrix.join(part[1])
            database.save()
        elif part[2].lower() == "irc":
            await send(room.room_id, f"Adding {part[1]} to the managed IRC channel list")
            database.add("irc", "managed", part[1])
            await client_irc.join(part[1])
            database.save()
        else:
            await send(room.room_id, "!join [room] [irc|matrix]\n")
    elif cmd == "leave" and len(part) == 3:
        if part[2].lower() == "matrix":
            await send(room.room_id, f"Removing {part[1]} from the managed matrix room list")
            database.remove("matrix", "managed", part[1])
            await client_matrix.room_leave(part[1])
            database.save()
        elif part[2].lower() == "irc":
            await send(room.room_id, f"Removing {part[1]} from the managed IRC channel list")
            database.remove("irc", "managed", part[1])
            await client_irc.leave(part[1])
            database.save()
        else:
            await send(room.room_id, "!leave [room] [irc|matrix]\n")
    elif cmd == "list":
        msg = "Managed matrix rooms:\n"
        for row in database.list_values("matrix", "managed"):
            msg += f"* {row}\n"
        msg += "Managed IRC channels:\n"
        for row in database.list_values("irc", "managed"):
            msg += f"* {row}\n"
        await send(room.room_id, msg)
    elif cmd == "sync":
        await sync(room)
    elif cmd == "save":
        database.save()
    elif cmd == "ban" and len(part) >= 2:
        user = part[1]
        reason = " ".join(part[2:])
        if reason == "":
            await send(room.room_id, f"Banning '{user}'")
            reason = None
        else:
            await send(room.room_id, f"Banning '{user}' for '{reason}'")

        if is_user_on_irc(user):
            ban_errors = {}
            successful_bans = 0
            for managed_channel in database.list_values("irc", "managed"):
                response = await client_irc.ban(managed_channel, get_irc_nick(user), reason)
                if response is not None:
                    ban_errors[managed_channel] = response
                    continue

                successful_bans += 1

            if successful_bans == 0 or len(ban_errors) > 0:
                message = f"Failed to ban '{user}':"
                for managed_channel, reason in ban_errors.items():
                    message += f"\n{managed_channel}: {reason}"
                await send(room.room_id, message)
        else:
            for room in database.list_values("matrix", "managed"):
                room_id = database.get_room_id(room)
                await client_matrix.room_ban(room_id, user, reason=reason)

    elif cmd == "kick" and len(part) >= 2:
        user = part[1]
        reason = " ".join(part[2:])
        if reason == "":
            await send(room.room_id, f"Kicking '{user}'")
            reason = None
        else:
            await send(room.room_id, f"Kicking '{user}' for '{reason}'")

        if is_user_on_irc(user):
            kick_errors = {}
            successful_kicks = 0
            for managed_channel in database.list_values("irc", "managed"):
                response = await client_irc.kick(managed_channel, get_irc_nick(user), reason)
                if response is not None:
                    kick_errors[managed_channel] = response
                    continue

                successful_kicks += 1

            if successful_kicks == 0 or len(kick_errors) > 0:
                message = f"Failed to kick '{user}':"
                for managed_channel, reason in kick_errors.items():
                    message += f"\n{managed_channel}: {reason}"

                await send(room.room_id, message)

        else:
            kick_errors = {}
            successful_kicks = 0
            for managed_room in database.list_values("matrix", "managed"):
                room_id = database.get_room_id(managed_room)
                response = await client_matrix.room_kick(room_id, user, reason=reason)

                if isinstance(response, RoomKickError):
                    # This error message is server implementation specific (Synapse, Dendrite) but Nio
                    # doesn't seem to have a better way to detect it
                    if (
                        response.message != "user does not belong to room"
                        and response.message != "The target user is not in the room"
                    ):
                        kick_errors[room_id] = response.message

                    continue

                successful_kicks += 1

            if successful_kicks == 0 or len(kick_errors) > 0:
                message = f"Failed to kick '{user}':"
                if len(kick_errors) == 0:
                    message += " The user doesn't seem to be in any of the managed rooms"
                else:
                    for room_id, reason in kick_errors.items():
                        message += f"\n{room_id}: {reason}"

                await send(room.room_id, message)

    elif cmd == "unban" and len(part) == 2:
        user = part[1]
        await send(room.room_id, f"Unbanning '{user}'")

        if not is_user_on_irc(user):
            for room in database.list_values("matrix", "managed"):
                room_id = database.get_room_id(room)
                await client_matrix.room_unban(room_id, user)
        else:
            for channel in database.list_values("irc", "managed"):
                await client_irc.unban(channel, get_irc_nick(user))

    elif cmd == "op" and len(part) == 2:
        user = part[1]
        await send(room.room_id, f"Giving '{user}' mod rights")

        if is_user_on_irc(user):
            await send(room.room_id, f"'{user}' is on IRC, give permissions using ChanServ/GroupServ")
        else:
            for room in database.list_values("matrix", "managed"):
                room_id = database.get_room_id(room)
                await power_level_update(room_id, user, 50)

    elif cmd == "deop" and len(part) == 2:
        user = part[1]
        await send(room.room_id, f"Removing '{user}' mod rights")
        if is_user_on_irc(user):
            await send(room.room_id, f"'{user}' is on IRC, remove permissions using ChanServ/GroupServ")
        else:
            for room in database.list_values("matrix", "managed"):
                room_id = database.get_room_id(room)
                await power_level_update(room_id, user, 0)

    elif cmd == "alias-add" and len(part) == 3:
        alias = part[1]
        repo = part[2]
        await send(room.room_id, f"Creating {alias}! and {alias}# pointing to {repo}")
        database.set_val("alias", alias, repo)
        database.save()

    elif cmd == "alias-del" and len(part) == 2:
        alias = part[1]
        await send(room.room_id, f"Removing {alias}! and {alias}#")
        database.remove("alias", alias)
        database.save()

    elif cmd == "aliases":
        message = ""
        for key in database.list_keys("alias"):
            repo = database.get("alias", key)
            message += f"{key} = {repo}\n"
        await send(room.room_id, message)

    elif cmd == "filehist":
        message = ""
        for file in file_history:
            message += str(file) + "\n"
        await send(room.room_id, message)

    elif cmd == "fileban" and len(part) == 2:
        hash = part[1]
        await send(room.room_id, f"Banning file hash {hash}")
        database.add("state", "fileban", hash)
        database.save()

    elif cmd == "help" or part[0][0] == "!":
        msg = (
            "Jonathan commands:\n"
            "!join [room] [irc|matrix]\n"
            "!leave [room] [irc|matrix]\n"
            "!list\n"
            "!sync\n"
            "!ban [user] [reason]\n"
            "!kick [user] [reason]\n"
            "!unban [user]\n"
            "!op [user]\n"
            "!deop [user]\n"
            "!alias-add [pma] [gitlab.com/postmarketOS/pmaports]\n"
            "!alias-del [pma]\n"
            "!aliases\n"
            "!filehist\n"
            "!fileban [hash]\n"
        )
        await send(room.room_id, msg)


async def run():
    global client_matrix
    global client_irc

    # IRC
    logging.info(
        f"Logging in as {config['connection']['irc_nick']} on {config['connection']['irc_network']}"
    )
    irc = miniirc.IRC(
        config["connection"]["irc_network"],
        6697, # There's no reason not to use an encrypted connection there.
        config["connection"]["irc_nick"],
        channels=database.list_values("irc", "managed"),
        ssl=True,
        ident="jonathan",
        realname=config["connection"]["irc_realname"],
        persist=True, # Automatically reconnect if it disconnects for any reason
        debug=False,
        ns_identity=config["connection"]["irc_password"],
        auto_connect=True,
        quit_message='Disconnecting'
    )
    irc.require('chans') # Require channel mode tracking from miniirc_extras

    client_irc = ClientIRC(irc)

    # Matrix
    client_matrix = AsyncClient(config["connection"]["matrix_server"], config["connection"]["matrix_account"])
    client_matrix.add_event_callback(on_invite, InviteMemberEvent)
    client_matrix.add_event_callback(on_message, RoomMessageText)
    client_matrix.add_event_callback(on_file, RoomMessageFile)
    client_matrix.add_event_callback(on_power_change, PowerLevelsEvent)

    logging.info(
        f"Logging in as {config['connection']['matrix_account']} on {config['connection']['matrix_server']}"
    )
    res = await client_matrix.login(config["connection"]["matrix_password"], device_name="jonathan")
    if isinstance(res, LoginError):
        logging.fatal(res)
        exit(1)
    else:
        logging.info(res)

    await client_matrix.sync_forever(timeout=30000)

def main():
    global config
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile")
    args = parser.parse_args()

    config = configparser.RawConfigParser()
    config.read(args.configfile)

    database.open_db(config["database"]["path"])

    asyncio.get_event_loop().run_until_complete(run())


if __name__ == "__main__":
    main()
