from time import sleep
import miniirc_extras # noqa: F401

class ClientIRC:
    def __init__(self, miniirc_irc):
        self.irc = miniirc_irc

    def op_up(self, channel):
        if self.irc.nick not in self.irc.chans[channel].modes.getset("o"):
            self.irc.msg("ChanServ", f"OP {channel}")
            i = 0
            while self.irc.nick not in self.irc.chans[channel].modes.getset("o") and i < 10:
                sleep(1)
                i += 1
            if self.irc.nick not in self.irc.chans[channel].modes.getset("o"):
                return "Unable to get op"

    async def join(self, channel):
        self.irc.send("JOIN", channel)

    async def ban(self, channel, user, reason):
        ret = self.op_up(channel)
        if ret:
            return ret

        self.irc.send("MODE", channel, "+b", f"{user}*!*@*") # Perhaps ban the IP instead?
        await self.kick(channel, user, reason)

    async def unban(self, channel, user):
        ret = self.op_up(channel)
        if ret:
            return ret

        self.irc.send("MODE", channel, "-b", f"{user}*!*@*")

    async def leave(self, channel):
        self.irc.send("PART", channel, "Leaving")

    async def send(self, target, message):
        self.irc.msg(target, message)

    async def kick(self, channel, user, reason):
        ret = self.op_up(channel)
        if ret:
            return ret

        if reason is None:
            self.irc.send("KICK", channel, user, "No reason")
        else:
            self.irc.send("KICK", channel, user, reason)

    async def op(self, channel, user):
        self.op_up(channel)
        self.irc.send("MODE", channel, "+o", user)
